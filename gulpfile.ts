import * as gulp from 'gulp';
import * as util from 'gulp-util';
import * as runSequence from 'run-sequence';

import Config from './tools/config';
import { loadTasks, loadCompositeTasks } from './tools/utils';

let root = 'src/client';
let yargs = require('yargs').argv,
  path = require('path'),
  template = require('gulp-template'),
  rename = require('gulp-rename');


loadTasks(Config.SEED_TASKS_DIR);
loadTasks(Config.PROJECT_TASKS_DIR);

loadCompositeTasks(Config.SEED_COMPOSITE_TASKS, Config.PROJECT_COMPOSITE_TASKS);


// --------------
// Clean dev/coverage that will only run once
// this prevents karma watchers from being broken when directories are deleted
let firstRun = true;
gulp.task('clean.once', (done: any) => {
  if (firstRun) {
    firstRun = false;
    runSequence('check.tools', 'clean.dev', 'clean.coverage', done);
  } else {
    util.log('Skipping clean on rebuild');
    done();
  }
});


// helper method to resolveToApp paths
let resolveTo = function (resolvePath: string) {
  return function (glob: any) {
    glob = glob || '';
    return path.join(root, resolvePath, glob);
  };
};

let resolveToApp = resolveTo('app'); // app/{glob}
let resolveToPages = resolveTo('app/pages'); // app/components/{glob}


// map of all our paths
let paths = {
  ts: resolveToApp('**/*.ts'),
  css: resolveToApp('**/*.css'),
  html: [
    resolveToApp('**/*.html'),
    path.join(root, 'index.html')
  ],
  blankTemplates: path.join(__dirname, 'generator', 'component/**/*.**'),
  blankPageTemplates: path.join(__dirname, 'generator', 'page/**/*.**'),
  blankServiceGroupTemplates: path.join(__dirname, 'generator', 'service_group/**/*.**'),
  blankServiceTemplates: path.join(__dirname, 'generator', 'service/*.js'),
  dist: path.join(__dirname, 'www/')
};


gulp.task('page', function () {
  let toCamelCase = function (str: string) {
    return str.replace(/^([A-Z])|[\s-_](\w)/g, function (match, p1, p2, offset) {
      if (p2) return p2.toUpperCase();
      return p1.toLowerCase();
    });
  };

  let toSpecial = function (oldStr: string, sperator: string) {
    return oldStr.replace(/[a-z][A-Z]/g, function (str, offset) {
      return str[0] + sperator + str[1].toLowerCase();
    });
  };


  let cap = function (val: string) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  };

  let name = yargs.name;
  let parentPath = yargs.parent || '';
  let preDirective = 'page-';
  let destPath = resolveToPages(name);
  let directive_name = parentPath.length > 0 ? parentPath + '/' : '';
  directive_name = preDirective + directive_name;
  directive_name += name;
  directive_name = directive_name.replace(/\//gi, '-');
  directive_name = toCamelCase(directive_name);
  let directive_ele = toSpecial(directive_name, '-');
  let module_name = toSpecial(directive_name, '.');

  return gulp.src(paths.blankPageTemplates)
    .pipe(template({
      name: name,
      directive_name: directive_name,
      directive_ele: directive_ele,
      module_name: module_name,
      upCaseName: cap(name)
    }))
    .pipe(rename(function (path: any) {
      path.basename = path.basename.replace('temp', name);
    }))
    .pipe(gulp.dest(destPath));
});

