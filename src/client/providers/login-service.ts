import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpService } from './http-service';


/*
 Generated class for the Login provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class LoginService {
  access_key: object;

  constructor(public HttpService: HttpService) {
  }

  login = function (data: object) {
    //get the response
    return this.HttpService.sendPost('/login/index', data);
  };

  signup = function (data: object) {
    return this.HttpService.sendPost('/signup/index', data);
  };


}
