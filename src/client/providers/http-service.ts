import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { StorageService } from './storage-service';
import 'rxjs/add/operator/toPromise';
import { URLSearchParams } from '@angular/http';


/*
 Generated class for the HttpService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class HttpService {
  DEFAULT_URL: string = 'http://localhost/DB_Server/index.php';
  // https://onememtk.000webhostapp.com/index.php
  // http://localhost/DB_Server/index.php
  constructor(public http: Http, public StorageService: StorageService) {
    console.log('Hello HttpService Provider');
  }


  /**
   * Use jquery to param serialize an object
   * @param data
   */
  paramSerialize = function (data: any) {
    let params = new URLSearchParams();

    for (let key in data) {
      params.set(key, data[key]);
    }
    return params.toString();
  };
  /**
   * Send normal post request
   * @param url
   * @param data
   */
  sendPost = function (url: string, data: any) {
    let headers = new Headers();
    headers.append('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');
    let options = new RequestOptions({headers: headers});
    url = this.DEFAULT_URL + url;
    return this.http.post(url, this.paramSerialize(data), options);

  };

  /**
   * Send normal get request
   * @param url
   * @param access_key
   */
  sendGet = function (url: string, access_key: any) {
    let headers = new Headers();
    headers.append('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');
    let options = new RequestOptions({headers: headers});
    let getParamString = 'user_name=' + access_key.user_name + '&access_token=' + access_key.access_token;
    url = this.DEFAULT_URL + url + '?' + getParamString;
    return this.http.get(url, options);
  };
  /**
   * Send get request with access key
   * Return Promise
   * @param url
   */
  sendGetAuth = function (url: string) {
    let access_key = this.StorageService.getAccessKey();
    return this.sendGet(url, access_key);
  };
  /**
   * Send post request only if the user is admin
   * @param url
   * @param data
   */
  sendPostAdminAuth = function (url: string, data: any) {
    let access_key = this.StorageService.getAccessKey();
    if (access_key.type === 'admin')
      return this.sendPost(url, data).map((res: any) => res.json()).toPromise();
  };
}
