import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { StorageService } from '../providers/storage-service';

/*
 Generated class for the UserService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UserService {

  constructor(public http: Http, public StorageService: StorageService) {
    console.log('Hello UserService Provider');
  }

  /**
   * Get all users data from the server
   * @returns {any} list of the users
   */
  getUser = function () {
    return this.HttpService.sendGetAuth({url: '/admin/list_user'});
  };

  editUser = function (parameters: { data: any }) {
    let data = parameters.data;
    return this.HttpService.sendPostAdminAuth({url: '/admin/edit_user', data: data});
  };
  deleteUser = function (parameters: { data: any }) {
    let data = parameters.data;
    return this.HttpService.sendPostAdminAuth({url: '/admin/delete_user', data: data});
  };

}
