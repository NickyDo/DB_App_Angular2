import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { LocalStorageService } from 'angular-2-local-storage/dist/local-storage.service';

/*
 Generated class for the StorageService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class StorageService {

  constructor(public storage: LocalStorageService) {
    console.log('Hello StorageService Provider');
  }

  /**
   * Set a key/value pair in the storage
   * @returns boolean
   * @param key
   * @param value
   */
  setValue(key: any, value: any) {
    return this.storage.set(key, value);
  }

  /**
   * Return the promise value corresponding to the key
   * @returns Return value of the key
   * @param key
   */
  getValue(key: any) {
    return this.storage.get(key);
  }

  /**
   * Remove a key value pair in the storage
   * @param parameters
   */
  removeKey(parameters: { key: any }) {
    let key = parameters.key;
    this.storage.remove(key);
  }

  /**
   * Clear all key value pairs in the storage
   * !!! HOT...
   */
  clearStorage() {
    this.storage.clearAll();
  }

  /**
   * Get the access key
   */
  getAccessKey = function () {
    return this.getValue('access_key');
  };

}
