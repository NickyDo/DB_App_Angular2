import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpService } from './http-service';
import { StorageService } from './storage-service';
import 'rxjs/add/operator/toPromise';
/*
 Generated class for the DataService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class DataService {

  constructor(public HttpService: HttpService, public StorageService: StorageService) {
    console.log('Hello DataService Provider');
  }

  getDataFromServer = function () {
    return this.HttpService.sendGetAuth('/stock/get_stock');
  };

  sendDataToServer = function (data: any) {
    return this.HttpService.sendPostAdminAuth('/stock/load_stock', data);
  };

  sendEditData = function (data: any) {
    return this.HttpService.sendPostAdminAuth('/stock/edit_stock', data);
  };
  sendDeletedData = function (data: any ) {
    return this.HttpService.sendPostAdminAuth('/stock/delete_stock', data);
  };
  /**
   * Send order data to server to save
   * Return Promise;
   */
  sendOrderToServer = function () {
    let access_key = this.StorageService.getAccessKey();
    let data = {
      'order': JSON.stringify(this.getOrder().then((res: any) => res)),
      'user_name': access_key.user_name
    };
    return this.HttpService.sendPost('/order/new_order', data).map((res:any) => res.json()).toPromise();
  };

  saveOrder = function (data: any ) {
    this.StorageService.setValue('order', data);
  };
  getOrder = function () {
    return this.StorageService.getValue('order');
  };

  deleteOrder = function () {
    this.StorageService.removeKey('order');
  };

  saveData = function (data: any ) {
    this.StorageService.setValue('data', data);
  };

  getData = function () {
    return this.StorageService.getValue('data');
  };

}
