import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
 Generated class for the UtilService provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UtilService {

  constructor() {
    console.log('Hello UtilService Provider');
  }

  parseJSON(parameters: { JSONObject: any }) {
    let JSONObject = parameters.JSONObject;
    return JSON.parse(JSONObject);
  }
}
