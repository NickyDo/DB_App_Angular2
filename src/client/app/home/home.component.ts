import { Component, Directive } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../providers/data-service';
import { StorageService } from '../../providers/storage-service';
import { SocketService } from '../../providers/socket-service';
/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  template: `
    <div class="row">
      <div class='col-md-6'>
        <div style='display: block;'>
          <canvas baseChart class='chart'
                  width='400' height='400'
                  [datasets]='lineChartData'
                  [labels]='lineChartLabels'
                  [options]='lineChartOptions'
                  [colors]='lineChartColors'
                  [legend]='lineChartLegend'
                  [chartType]='lineChartType'>
          </canvas>
        </div>
      </div>
    </div> `,
  styleUrls: ['home.component.css']
})
export class HomeComponent {
  data: object[];
  maxTicks: number = 15;
  public lineChartData: Array<any> = [
    { data: [], label: 'Emotion' }
  ];
  public lineChartLabels: Array<string> = [];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          position: 'left',
          ticks: {
            type: 'linear',
            id: 'value',
            display: true,
            position: 'left',
          }
        }
      ],
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            id: 'datetime',
            type: 'linear',
            display: true,
            position: 'bottom',
            maxTicksLimit: this.maxTicks,
            maxRotation: 0
          }
        }
      ]
    }
  };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  constructor(private router: Router, public DataService: DataService,
    public StorageService: StorageService, public SocketService: SocketService) {
    // if (this.StorageService.getAccessKey()) {
    //   this.getData();
    // } else {
    //   this.gotoLoginPage();
    // }

    this.SocketService.getMessage().subscribe((response: any) => {
      //a trick...to refresh data
      // this.lineChartData[0].data.push(response.emotion);
      // this.lineChartLabels.push(this.epochToLocaleTimeString(response.time));

      this.loadGraphData(this.lineChartData[0].data, response.emotion);
      this.loadGraphLabels(this.lineChartLabels, response.time);
      this.lineChartData = this.lineChartData.slice();      
      console.log(this.lineChartData[0].data, this.lineChartLabels);
    });
  }

  loadGraphData(previousData: number[], newData: number) {
    if (previousData.length >= this.maxTicks) {
      previousData.splice(0, 1);
    }
    previousData.push(newData);
    this.lineChartData[0].data = previousData;
  }

  loadGraphLabels(previousData: string[], newData: string) {
    if (previousData.length >= this.maxTicks) {
      previousData.splice(0, 1);
    }
    previousData.push(newData);
    this.lineChartLabels = previousData;
  }

  epochToLocaleTimeString(epochTime_Mils: number) {
    let d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setMilliseconds(epochTime_Mils);
    return d.toLocaleTimeString();
  }

  goToAllGamePage() {
    this.router.navigate(['/allGame']);
  }

  gotoLoginPage() {
    this.router.navigate(['/login']);
  }

  /**
   * First get data from storage.
   * If failed, call the API to get data from server
   */
  getData() {
    // Get data from storage
    // this.data = this.DataService.getData();
    // if (this.data === null) {
    this.getDataFromServer();
    // }
  }

  getDataFromServer() {
    this.DataService.getDataFromServer().subscribe((response: any) => {
      this.data = JSON.parse(response._body);
      //save the response data to storage
      this.DataService.saveData(this.data);
    });
  }
}
