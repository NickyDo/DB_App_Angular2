import { Component } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})
export class NavbarComponent {
  pages: object [];

  constructor() {
    this.pages = [
      {'path': '/', 'name': 'HOME'},
      {'path': '/about', 'name': 'ABOUT'},
      {'path': '/allGame', 'name': 'ALLGAME'},
      {'path': '/login', 'name': 'LOGIN'}
    ];
  }
}
