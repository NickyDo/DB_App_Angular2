import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LocalStorageModule } from 'angular-2-local-storage/dist/local-storage.module';
import { SocketIoModule, SocketIoConfig } from 'ng2-socket-io/index';
//should not shorten the ng2-socket-io
import { ChartsModule } from 'ng2-charts/ng2-charts';

// For pages
import { SharedModule } from './shared/shared.module';
import { AboutModule } from './about/about.module';
import { HomeModule } from './home/home.module';
import { AllGameModule } from './allGame/allGame.module';
import { LoginModule } from './login/login.module';

// For providers
import { HttpService } from '../providers/http-service';
import { DataService } from '../providers/data-service';
import { LoginService } from '../providers/login-service';
import { UserService } from '../providers/user-service';
import { StorageService } from '../providers/storage-service';
import { UtilService } from '../providers/util-service';
import { SocketService } from '../providers/socket-service';

const config: SocketIoConfig = {url: 'http://localhost:3000/', options: {}};
//http://localhost:3000/


@NgModule({
  imports: [BrowserModule, HttpModule, AppRoutingModule,
    FormsModule, SocketIoModule, ChartsModule,
    AboutModule, HomeModule, AllGameModule, LoginModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    }),
    SocketIoModule.forRoot(config),
    SharedModule.forRoot()],
  declarations: [AppComponent],
  providers: [
    HttpService,
    DataService,
    LoginService,
    UserService,
    StorageService,
    UtilService,
    SocketService,
    {
      provide: APP_BASE_HREF,
      useValue: '<%= APP_BASE %>'
    }],
  bootstrap: [AppComponent]

})
export class AppModule {
}
