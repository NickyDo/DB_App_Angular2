import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AllGameComponent } from './allGame.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'allGame', component: AllGameComponent }
    ])
  ],
  exports: [RouterModule]
})
export class AllGameRoutingModule { }
