import { Component, OnInit } from '@angular/core';
import { DataService } from '../../providers/data-service';
import { Router } from '@angular/router';

@Component({
  selector: 'sd-all-game',
  templateUrl: 'app/allGame/allGame.component.html',
  styleUrls: ['app/allGame/allGame.component.css'],
})
export class AllGameComponent implements OnInit {
  data: object[];

  constructor(private router: Router, public DataService: DataService) {
  }

  ngOnInit() {
    this.getDataFromStorage();
  }

  /**
   * find index of product in currentOrder
   * @param arraytosearch
   * @param valuetosearch
   * @returns {any}
   */
  findIndexOfProduct(arraytosearch: any, valuetosearch: any) {
    for (var i = 0; i < arraytosearch.length; i++) {
      if (arraytosearch[i] === valuetosearch) {
        return i;
      }
    }
    return null;
  }

  purchaseGame(product: any) {
    let currentOrder = this.DataService.getOrder();
    // let indexOfParams = currentOrder.indexOf(product);
    let indexOfParams = this.findIndexOfProduct(currentOrder, product);
    // console.log(indexOfParams);
    if (indexOfParams > -1) {
      this.data.map((item: any) => {
        if (item['stock_name'] === product.stock_name) {
          item['stock_quantity']++;
          return item;
        }
      });
    } else {
      product['stock_quantity'] = 1;
      currentOrder.push(product);
    }
    this.DataService.saveOrder(currentOrder);
  }

  getDataFromStorage() {
    this.data = this.DataService.getData();
  }

}
