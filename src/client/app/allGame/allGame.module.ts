import { NgModule } from '@angular/core';
import { AllGameComponent } from './allGame.component';
import { AllGameRoutingModule } from './allGame-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [AllGameRoutingModule, SharedModule],
  declarations: [AllGameComponent],
  exports: [AllGameComponent],
  providers: []
})
export class AllGameModule { }
