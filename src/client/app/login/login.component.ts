import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../providers/storage-service';
import { LoginService } from '../../providers/login-service';

@Component({
  moduleId: module.id,
  selector: 'sd-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {
  login_username: string;
  login_password: string;

  constructor(public router: Router, public StorageService: StorageService, public LoginService: LoginService) {
  }


  login() {
    let params = {
      'login_username': this.login_username,
      'login_password': this.login_password
    };
    this.LoginService.login(params).subscribe((res: any) => {
      let resObj = JSON.parse(res._body);
      if (resObj.status) {
        //login successfully
        let saveData = {
          'user_name': resObj.user_name,
          'type': resObj.type,
          'access_token': resObj.access_token
        };
        //save the log in key
        this.saveUserKey(saveData);
        this.goToHomePage();
      } else {
        //if the username or password is incorrect
        console.log('Wrong password');
      }
    }, (error: any) => {
      //if we have unkown error
      console.log(error);
    });
  }

  goToHomePage() {
    this.router.navigate(['/']);
  }

  saveUserKey(saveData: any) {
    this.StorageService.setValue('access_key', saveData);
  }
}
