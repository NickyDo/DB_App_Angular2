import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { <%= upCaseName %>Component } from './<%= name %>.component';
import { <%= upCaseName %>RoutingModule } from './<%= name %>-routing.module';

@NgModule({
  imports: [CommonModule, <%= upCaseName %>RoutingModule],
  declarations: [<%= upCaseName %>Component],
  exports: [<%= upCaseName %>Component]
})
export class <%= upCaseName %>Module { }
