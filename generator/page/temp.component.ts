import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-<%= name %>',
  templateUrl: '<%= name %>.component.html',
  styleUrls: ['<%= name %>.component.css']
})
export class <%= upCaseName %>Component { }
