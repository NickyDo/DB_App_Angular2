import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { <%= upCaseName %>Component } from './<%= name %>.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '<%= name %>', component: <%= upCaseName %>Component }
    ])
  ],
  exports: [RouterModule]
})
export class <%= upCaseName %>RoutingModule { }
